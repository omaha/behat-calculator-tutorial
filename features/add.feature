Feature: Add two numbers

  Scenario: Add two positive numbers
    Given I am in "/var/www/html/calculator" 
    When I enter "bin/cake calculator add 2 5" 
    Then I should get "7"