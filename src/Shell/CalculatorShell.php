<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Shell;

/**
 * Description of CalculatorShell
 *
 * @author markus
 */
class CalculatorShell extends \Cake\Console\Shell {

    public function _welcome() {
    }
    
    public function main() {
        $operation = $this->args[0];
        $summand1 = $this->args[1];
        $summand2 = $this->args[2];
        if($operation == "add"){
            $this->out($summand1 + $summand2);
        }
    }

}
